# Kafka-configurator

Kafka-configurator is a simple script that applies topic-level configuration and ACLs, defined in a static configuration file, onto a live Kafka cluster.

It aims at being as simple as possible, and doing one thing right.


## Configuration

The default location for `kafka-configurator`'s own configuration file is `/etc/kafka-configurator/config.yaml`, and should be structured as following

```yaml
cluster:
  bootstrap: localhost:9092

features:
  create_or_update:
    config: true   # Whether to apply/update topic level configuration
    acls: false    # Whether to apply/updates ACLs
  delete_extra:
    config: false  # Whether to remove all topic-level configurations not specified in static config
    acls: false    # Whether to remove all topic-level ACLs not specified in static config

topics:
  topic1:
    config:
      retention.ms: '86400000'
  topic3:
    config:
      retention.ms: '86400000'

```
